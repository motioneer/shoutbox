<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Str;
use App\Message;

class ApiInputTest extends TestCase {

    use RefreshDatabase;

    public function setUp(): void {
        parent::setUp();
        $this->seed();
    }

    /**
     * Check if IP is logged
     * @test
     * @return void
     */
    public function ipLogging() {
        $name = Str::random(32);
        $message = Str::random(512);
        $response = $this->postJson('/api/message', [
            'name' => $name,
            'message' => $message
        ]);
        $ip = Message::latest()->first()->ip;
        $this->assertRegExp('/^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/', $ip);
    }

    /**
     * Check if User Agent is logged
     * @test
     * @return void
     */
    public function userAgentLogging() {
        $name = Str::random(32);
        $message = Str::random(512);
        $response = $this->postJson('/api/message', [
            'name' => $name,
            'message' => $message
        ]);
        $this->assertNotEmpty(Message::latest()->first()->user_agent);
    }

}
