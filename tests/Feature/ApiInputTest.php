<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Str;
use App\Message;

class ApiMetadataTest extends TestCase {

    use RefreshDatabase;

    public function setUp(): void {
        parent::setUp();
        $this->seed();
    }

    /**
     * Basic Get Messages Route Response
     * @test
     * @return void
     */
    public function getMessages() {

        $response = $this->getJson('/api/messages');
        $response->assertStatus(200);
    }

    /**
     * Basic Post Messages
     * @test
     * @return void
     */
    public function postMessage() {
        $name = Str::random(32);
        $message = Str::random(512);
        $response = $this->postJson('/api/message', [
            'name' => $name,
            'message' => $message
        ]);
        $response->assertStatus(200);
        $this->assertDatabaseHas('messages', [
            'name' => $name,
            'message' => $message
        ]);
    }

    /**
     * Post no Name
     * @test
     * @return void
     */
    public function postNoName() {
        $message = Str::random(512);
        $response = $this->postJson('/api/message', [
            'name' => '',
            'message' => $message
        ]);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('messages', [
            'message' => $message
        ]);
    }

    /**
     * Post no Message
     * @test
     * @return void
     */
    public function postNoMessage() {
        $name = Str::random(32);
        $response = $this->postJson('/api/message', [
            'name' => $name,
            'message' => ''
        ]);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('messages', [
            'name' => $name
        ]);
    }

    /**
     * Post to long Name
     * @test
     * @return void
     */
    public function postToLongName() {
        $name = Str::random(33);
        $message = Str::random(512);
        $response = $this->postJson('/api/message', [
            'name' => $name,
            'message' => $message
        ]);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('messages', [
            'name' => $name,
            'message' => $message
        ]);
    }

    /**
     * Post to long Message
     * @test
     * @return void
     */
    public function postToLongMessage() {
        $name = Str::random(32);
        $message = Str::random(513);
        $response = $this->postJson('/api/message', [
            'name' => $name,
            'message' => $message
        ]);
        $response->assertStatus(422);
        $this->assertDatabaseMissing('messages', [
            'name' => $name,
            'message' => $message
        ]);
    }

    /**
     * Max 5 Messages
     * @test
     * @return void
     */
    public function maxMessage() {
        $count = Message::all()->count();
        $this->assertLessThanOrEqual(5, $count);
    }

    /**
     * Max 5 Messages after posting
     * @test
     * @return void
     */
    public function maxMessageAfterPost() {
        $response = $this->postJson('/api/message', [
            'name' => 'John',
            'message' => 'Hello!'
        ]);
        $count = Message::all()->count();
        $this->assertLessThanOrEqual(5, $count);
    }

}
