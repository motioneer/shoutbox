# Willkommen bei Shoutbox!

Dies ist eine Beispielapplikation für die Verwendung von React und PHP.


# Requirements

 - PHP ^7.2 - https://www.php.net/downloads
 - Composer - https://getcomposer.org/download/
 - Laravel ^6  - https://laravel.com/docs/master
 - mySQL - https://www.apachefriends.org/de/index.html
 - nodeJs ^12 - https://nodejs.org/en/download/
 - npm ^6 - https://www.npmjs.com/get-npm

# Installation
## 1)  **Datenbank konfigurieren**
Im root Folder liegt die *.env.example* Datei, diese unter dem Namen *.env* kopieren. In dieser müssen folgende Attribute auf die lokale mySQL Datenbank angepasst werden:

1. DB_HOST
2. DB_PORT
3. DB_DATABASE
4. DB_USERNAME
5. DB_PASSWORD

## 2) **Backend starten**
In einer Command Line den root Folder navigieren und folgende Commands ausführen:
```bash
composer install
php artisan key:generate
php artisan migrate:refresh --seed
php artisan serve
```
Die Command Line zeigt nun die IP und Port an.

## 3) **Frontend konfigurieren**
Sollte die Backend IP und Port abweichen von **localhost:8000** oder **127.0.0.1:8000** muss die Frontend Konfiguration angepasst werden.

Diese Backend IP und Port müssen im Frontend angepasst werden, im File:
**/frontend/src/ApiRoutes**

```javascript
const baseUrl = 'http://127.0.0.1:8000/api/';
```
Hier **127.0.0.1:8000** mit der tatsächlichen IP und Port ersetzten.
Der **/api** Teil der URL muss dabei erhalten bleiben!

## 4) **Frontend starten**
In einer separaten Command Line in den **/frontend** Folder navigieren und folgende Commands ausführen:
```bash
npm install
npm start
```
Die Command Line gibt nun die Adresse aus unter der das Frontend läuft.



# Tests

Um die Tests laufen zu lassen, mit einer Command Line in den root Folder navigieren und folgenden Befehl ausführen:

```bash
vendor\bin\phpunit
```

