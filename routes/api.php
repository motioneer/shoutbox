<?php

use Illuminate\Http\Request;
use App\Message;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::get('messages', function() {
    return Message::orderBy('updated_at')->get();
});

Route::post('message', function(Request $request) {

    $validatedData = $request->validate([
        'name' => 'required|max:32',
        'message' => 'required|max:512',
    ]);

    // create new Message
    $message = new Message();
    $message->name = $validatedData['name'];
    $message->message = $validatedData['message'];
    $message->ip = $request->ip();
    $message->user_agent = $request->header('User-Agent');
    $message->save();

    // delete all but the latest 5 messages
    $count = Message::count();
    $deleteUs = Message::latest()->take($count)->skip(5)->get();
    foreach ($deleteUs as $deleteMe) {
        Message::where('id', $deleteMe->id)->delete();
    }


    return response('', 200);
});
