const baseUrl = 'http://127.0.0.1:8000/api/';

export const getMessagesRoute = baseUrl+'messages';
export const postMessageRoute = baseUrl+'message';