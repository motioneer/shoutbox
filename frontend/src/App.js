import React from 'react';
import './App.css';
import 'typeface-roboto';
import Shoutbox from './Shoutbox';
import store from './store/index';
import { Provider } from "react-redux";
import Poller from './poller';

function App() {
    return (
            <Provider store={store}>
                <div className="App">
                    <Shoutbox/>
                </div>
            </Provider>
            );
}

export default App;
