import store from './store/index';
import { fetchShouts } from './store/actions';

function poll() {
    store
            .dispatch(fetchShouts())
            .then(() => console.log(store.getState()));
}
// poll on load
poll();
const Poller = setInterval(poll, 3000);

export default Poller;