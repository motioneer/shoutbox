import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { connect } from "react-redux";
import SubmitBox from './SubmitBox';
import Shout from './Shout';


const useStyles = makeStyles(theme => ({
        root: {
            margin: 20,
            flexGrow: 1
        },
        title: {
            flexGrow: 1
        }
    }));

const mapStateToProps = state => {
    return {shouts: state.shouts};
};

function ConnectedShoutbox( {shouts}) {
    const classes = useStyles();
    return(
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h4" className={classes.title}>
                            Shoutbox
                        </Typography>
                    </Toolbar>
                </AppBar>
                {
                    shouts.map((shout, i) =>
                        <Shout key={i} shout={shout} />
                    )
                }
                <SubmitBox />
            </div>
            );
}

const Shoutbox = connect(mapStateToProps)(ConnectedShoutbox);
export default Shoutbox;