import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { updateName, sendShouts } from './store/actions';
import { makeStyles } from '@material-ui/core/styles';
import { connect } from "react-redux";

const useStyles = makeStyles(theme => ({
        form: {
            display: 'flex',
            flexWrap: 'wrap',
            justifyContent: 'space-between',
            marginTop: '1em',
            width: '100%',
        },
        nameField: {
            flexShrink: 1,
            flexGrow: 2,
            margin: '0.2em'
        },
        messageField: {
            flexGrow: 3,
            margin: '0.2em'

        },
        button: {
            margin: '0.2em'
        }
    }));

const mapStateToProps = state => {
    return {name: state.name};
};

function mapDispatchToProps(dispatch) {
    return {
        updateName: e => dispatch(updateName(e.target.value)),
        sendShout: e => {
            e.preventDefault();
            if (e.target.name.value && e.target.message.value) {
                dispatch(sendShouts({
                    name: e.target.name.value,
                    message: e.target.message.value
                }));
                //empty Message input field
                e.target.message.value = '';
            }
        }
    };
}

function ConnectedSubmitBox(props) {
    const classes = useStyles();
    return(
            <form  
                noValidate 
                autoComplete="off" 
                className={classes.form}
                onSubmit={props.sendShout}
                >
                <TextField 
                    value={props.name} 
                    className={classes.nameField} 
                    inputProps={{maxLength: 32}} 
                    required
                    id="name" 
                    type="text"
                    label="Your Name" 
                    variant="outlined" 
                    onChange={props.updateName}
                    />
                <TextField 
                    className={classes.messageField}
                    id="message"
                    type="text"
                    inputProps={{maxLength: 512}} 
                    required
                    label="Message" 
                    variant="outlined"  
                    />
                <Button
                    className={classes.button}
                    color="primary"
                    variant="outlined" 
                    type="submit"
                    >shout</Button>
            </form>
            );


}

const SubmitBox = connect(mapStateToProps, mapDispatchToProps)(ConnectedSubmitBox);
export default SubmitBox;