import {
UPDATE_NAME,
        RECEIVE_SHOUTS,
        FETCH_SHOUTS,
        SEND_SHOUT
        } from './actions';

const initialState = {
    name: '',
    shouts: []
};

export default function shoutbox(state = initialState, action) {
    switch (action.type) {
        case UPDATE_NAME:
            return Object.assign({}, state, {
                name: action.name
            });
        case FETCH_SHOUTS:
            return Object.assign({}, state, {
                isFetching: true
            })
        case RECEIVE_SHOUTS:
            return Object.assign({}, state, {
                isFetching: false,
                shouts: action.shouts,
                lastUpdated: action.receivedAt
            })
        case SEND_SHOUT:
            return Object.assign({}, state, {
               
            });
        default:
            return state;
}
}