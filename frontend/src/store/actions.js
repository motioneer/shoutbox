import fetch from 'cross-fetch'
import {getMessagesRoute, postMessageRoute} from '../ApiRoutes'

        /*
         * action types
         */
export const UPDATE_NAME = 'UPDATE_NAME';
export const FETCH_SHOUTS = 'FETCH_SHOUTS';
export const RECEIVE_SHOUTS = 'RECEIVE_SHOUTS';
export const SEND_SHOUT = 'SEND_SHOUT';
/*
 * other constants
 */

/*
 * action creators
 */
export function updateName(name) {
    return {type: UPDATE_NAME, name};
}

export function receiveShouts(json) {
    return {
        type: RECEIVE_SHOUTS,
        shouts: json,
        receivedAt: Date.now()
    };
}

export function fetchShouts() {
    return function (dispatch) {
        return fetch(getMessagesRoute)
                .then(
                        response => response.json(),
                        error => console.log('An error occurred.', error)
                )
                .then(json => dispatch(receiveShouts(json)));
    };
}

export function sendShouts(shout) {
    return function (dispatch) {
        return fetch(postMessageRoute, {
            method: 'post', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: shout.name,
                message: shout.message
            })
        })
                .then(
                        response => response,
                        error => console.log('An error occurred.', error)
                )
                .then(() => dispatch(fetchShouts()));
    };
}
