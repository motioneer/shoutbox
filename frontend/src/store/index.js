import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import shoutbox from "./reducers";

const store = createStore(
        shoutbox,
        applyMiddleware(
                thunkMiddleware
                ));
      
export default store;