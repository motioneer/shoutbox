import React from 'react';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';
import Linkify from 'react-linkify';

const useStyles = makeStyles(theme => ({
        shout: {
            marginTop: '1em',
            padding: '1em',
            textAlign: 'left',
            wordBreak: 'break-all'
        }
    }));


export default function Shout(props) {
    const classes = useStyles();
    return(
            <Paper className={classes.shout}>
                <Typography variant="h5" className={classes.name} >
                    {props.shout.name}
                </Typography>
                <Typography variant="body1"  className={classes.body}>
                    <Linkify>
                        {props.shout.message}
                    </Linkify>
                </Typography>
            </Paper>
            )
}