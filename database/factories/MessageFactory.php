<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Message;
use Faker\Generator as Faker;

$factory->define(Message::class, function (Faker $faker) {
    $creationDate=$faker->dateTimeInInterval('-1 days', '6 hours');
    return [
        'name' => $faker->randomElement([
            $faker->name,
            $faker->firstName
        ]),
        'message' => $faker->randomElement([
            $faker->realText(160),
            $faker->realText(80),
            $faker->url
        ]),
        'ip' => $faker->ipv4,
        'user_agent' => $faker->userAgent,
        'created_at' => $creationDate,
        'updated_at' => $creationDate
    ];
});
